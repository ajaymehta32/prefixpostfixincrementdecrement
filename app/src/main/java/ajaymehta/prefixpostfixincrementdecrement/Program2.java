package ajaymehta.prefixpostfixincrementdecrement;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

// see the effect of  prefix and postfix increment in a statement..
public class Program2 {

    static int o = 10;

    public static void main(String args[]) {

        int n =14;  // we cant do 14++

      int a = 2 * ++o;  // it is first incremented to 11 then multiplied by 2

        int b = 2* n++;  // it is mulitpliyed by 2 -- >  2*14 = 28  then it is incremented to 15

        System.out.println(a + " "+b+ " "+n);
    }
}
