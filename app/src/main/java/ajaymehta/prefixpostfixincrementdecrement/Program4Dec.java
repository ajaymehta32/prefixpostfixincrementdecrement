package ajaymehta.prefixpostfixincrementdecrement;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

// see the effect of  prefix and postfix increment in a statement..
public class Program4Dec {

    static int o = 10;

    public static void main(String args[]) {

        int n =14;  // we cant do 14++

      int a = 2 * --o;  // it is first decremented to 9 then multiplied by 2 ->18

        int b = 2* n--;  // it is mulitpliyed by 2 -- >  2*14 = 28  then it is decrement to 13

        System.out.println(a + " "+b+ " "+n);
    }
}
