package ajaymehta.prefixpostfixincrementdecrement;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

public class Program1 {

    static int o = 10;

    public static void main(String args[]) {

        int n =14;  // we cant do 14++

        n++;
        ++o;    // prefix postfix doesnt matter here ..they both are working same..the effect of them gets to shown when they use in statements..

        System.out.println(n +" "+ o);
    }
}
