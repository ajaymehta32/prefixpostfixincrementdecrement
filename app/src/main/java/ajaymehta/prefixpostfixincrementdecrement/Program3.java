package ajaymehta.prefixpostfixincrementdecrement;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

public class Program3 {

    public static void main(String args[]) {  // see even in loop it doesnt effect ...it will still print 0 first then it will increment..
        // coz we know for and while loop works as same

        for(int i=0; i<=10; ++i) {   // 1 n 2 statemnt will execute ..   initilize  n  check if it is less then 10

            System.out.println(i);   // then print our statemt    // 3 then increent ++i ( will be executed )
        }
    }
}
  /*
  *   int i=1;        first initilize
  *   while(i<10) {     then check ..till it is less then 10 it gonna be looping
  *
  *   System.out.print(i);   // then print statement
  *   i++;                    //  then increment ..
  *   }
  *
  * */